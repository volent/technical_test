""" Pizza factory yeaaah !

In this test you will have to use threading, builtins and OOP.

You are expected to comment your code like you would if it was a serious
project.

You are free to use any documentation, here are some links :

http://www.google.com

The project PizzaFactory is about Pizzas.

The goal is to cook every pizza we have at the beginning of the main function.
The output should be (in any order) :

    Pesto Gusto is cooking for 5 seconds.
    Seafood Symphony is cooking for 5 seconds.
    Tropical Dream is cooking for 5 seconds.
    Quatro Cheese Deluxe is cooking for 5 seconds.

Good luck.
"""
from threading import Thread

class Pizza(object):
    def __init__(self, name, cooking_time=5):
        self.name       = name
        self.cooking_time = cooking_time
        self.cooked = False
    
    def cook(self):
        print "{} is cooking for {} seconds".format(
            self.name, self.cooking_time
        )
        sleep(self.cooking_time)
        self.cooked = True
   
class Oven(Thread):
    def __init__(self):
        self.content = None
        self.running = False

    def run(self):  
        if self.content:  
            self.content.cook()

class Pizzaiolo(object):
    def __init__(self):
        pass

    def put_pizza_in_oven(self, pizza, oven):
        oven.content = pizza


class PizzaFactory(object):
    def __init__(self, pizzaiolo, ovens):
        self.ovens      = ovens
        self.pizzaiolo  = pizzaiolo

    def make_pizza(self, pizza):
        for oven in self.ovens:
            if not oven.running: self.pizzaiolo.put_pizza_in_oven(pizza, oven)


def main():
    pizzas = [Pizza("Pesto Gusto"), Pizza("Seafood Symphony"), Pizza("Tropical Dream"), Pizza("Quatro Cheese Deluxe")]
    oven = Oven()
    pizzaiolo = Pizzaiolo()
    factory = PizzaFactory(pizzaiolo, [oven, oven, oven])
    for pizza in pizzas:
        if pizza.cooked == False:
            factory.make_pizza(pizza)

if __name__ == "__main__":
    main()


     
