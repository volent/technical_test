<?php
/*
 * Movies yeaaah !
 *
 * In this test you will have to use arrays !
 *
 * You are expected to comment your code like you could if it was a serious
 * project.
 *
 * You are free to use any documentation, here are some links :
 * 
 * http://www.google.com
 *
 * The project will be made of multiple small tests.
 */


$movies = array(
    "Game of Thrones",
    "Gone Girl",
    "Dracula",
    "Interstellar",
    "Friends",
    "Equalizer",
    "The Dark Knight",
    "House of Cards"
);

//         - Test 1 -
// Print the $movies array to stdout.


//         - Test 2 -
// Print the sorted $movies array to stdout.


//         - Test 3 -
// Print the sorted $movies array to stdout.


//         - Test 4 -
// Can you use both single quotes (' ') and double quotes (" ") for strings ?


//         - Test 5 -
// Fix the following statement :

$query = "SELECT * FROM user WHERE username = '".$_POST['username']."'";

//         - Test 6 -
// Write a small function (less than 25 lines / 80 columns) that will blow my mind


?>