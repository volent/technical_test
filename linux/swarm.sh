# Write a script that duplicates itself at the end of every file ending in .sh
# in the current directory. It should still duplicate itself and only itself if
# the infected file are ran.
#
# Example :
#
#    $ ls
#    swarm.sh test.sh
#    $ cat test.sh
#    echo "Hello World !"
#    $ cat swarm.sh
#    --- content ---
#    $ sh swarm.sh
#    $ cat test.sh
#    echo "Hello World !"
#    --- content ---
#    $ echo "ls -la" > test2.sh
#    $ sh test.sh
#    $ cat test2.sh
#    ls -la
#    --- content ---
