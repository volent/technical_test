# Technical test

## Introduction

In order to participate you must fix the code in a **private** repository (that's the reason why it's on bitbucket and not on github, free private repository yeeeeah) and share it with me.

There are four tests in the repository : Python, HTML5/CSS3/Php, Linux & Git.

The git test will simply be an analysis of your commits, we don't want you to be a pro but we expect you to know how to use branches, merge and rebase.

As you can see, the test is written in English and you should write your code in English too.
